package com.example.jharshman.jdharshmanlab9;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Calendar;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private float mZoom = 15;
    private GoogleMap mMap;
    private LocationManager mLocationManager;
    private String mProvider;
    private LatLng mCoord;
    private Button mTypeButton;
    private Button mMarkerButton;
    private int mMarkCount = 0;
    private ArrayList<MarkerOptions> mMarkerOptList;
    private ArrayList<Marker> mMarkerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapFragment.setHasOptionsMenu(true);

        mMarkerButton = (Button)findViewById(R.id.mark_button);
        mTypeButton = (Button)findViewById(R.id.type_button);
        mTypeButton.setText(R.string.label_satellite);
        mMarkerList = new ArrayList<>();

        if(savedInstanceState != null) {
            mZoom = savedInstanceState.getFloat("zoom");
            mMarkCount = savedInstanceState.getInt("count");
            mMarkerOptList = (ArrayList)savedInstanceState.getSerializable("markers");
        } else {
            mMarkerOptList = new ArrayList<>();
        }

        //long click listener for marker button
        mMarkerButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //remove last marker from list
                if (!mMarkerList.isEmpty()) {
                    Marker cur = mMarkerList.get(mMarkCount - 1);
                    mMarkerList.remove(mMarkCount - 1);
                    cur.remove();
                    mMarkCount--;
                } else {
                    Toast toast = Toast.makeText(v.getContext(), "No Marker To Remove", Toast.LENGTH_SHORT);
                    toast.show();
                }
                return true;
            }
        });

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);

        try {
            initMap();
        } catch (SecurityException se) {
            Toast toast = Toast.makeText(this, "No Permissions", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_about) {
            aboutMe();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("markers", mMarkerOptList);
        outState.putFloat("zoom", mZoom);
        outState.putInt("count", mMarkCount);
    }

    /**
     * */
    private void initMap() throws SecurityException {
        mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        mProvider = getProvider();

        Location recentLocation = mLocationManager.getLastKnownLocation(mProvider);

        if(recentLocation == null) {
            Toast toast = Toast.makeText(this, "Null Location", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            mCoord = new LatLng(recentLocation.getLatitude(), recentLocation.getLongitude());
        }

        // restore any marks
        if(!mMarkerOptList.isEmpty()) {
            for(MarkerOptions mo : mMarkerOptList) {
                Marker marker = mMap.addMarker(mo);
                mMarkerList.add(marker);
            }
        }

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mCoord, mZoom));
    }

    /**
     * */
    public void onPlus(View v) {
        mMap.moveCamera(CameraUpdateFactory.zoomIn());
        mZoom = mMap.getCameraPosition().zoom;
    }

    /**
     * */
    public void onMinus(View v) {
        mMap.moveCamera(CameraUpdateFactory.zoomOut());
        mZoom = mMap.getCameraPosition().zoom;
    }

    /**
     * */
    public void typeChange(View v) {
        int mapType = mMap.getMapType();
        switch(mapType) {
            case GoogleMap.MAP_TYPE_NORMAL:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                mTypeButton.setText(R.string.label_terrain);
                break;
            case GoogleMap.MAP_TYPE_SATELLITE:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                mTypeButton.setText(R.string.label_normal);
                break;
            case GoogleMap.MAP_TYPE_TERRAIN:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mTypeButton.setText(R.string.label_satellite);
                break;
        }


    }

    /**
     * */
    public void placeMark(View v) throws SecurityException {

        // get current time
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR);
        int min = calendar.get(Calendar.MINUTE);
        int sec = calendar.get(Calendar.SECOND);

        Location location = mLocationManager.getLastKnownLocation(mProvider);
        LatLng current = new LatLng(location.getLatitude(), location.getLongitude());

        MarkerOptions markerOptions = new MarkerOptions()
                .position(current)
                .title("Mark #" + (++mMarkCount))
                .snippet("My Location @ " + hour + ":" + min + ":" + sec)
                .icon(
                        BitmapDescriptorFactory.fromResource(R.drawable.mapmarker)
                );

        Marker marker = mMap.addMarker(markerOptions);
        mMarkerList.add(marker);
        mMarkerOptList.add(markerOptions);
    }

    /**
     * */
    private String getProvider() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setSpeedRequired(true);

        String providerName = mLocationManager.getBestProvider(criteria, true);
        if(providerName != null) {
            return providerName;
        } else {
            return LocationManager.GPS_PROVIDER;
        }
    }

    /**
     * */
    private void aboutMe() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
        builder.setTitle(R.string.attr_dialog_title)
                .setItems(R.array.about_me, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.create();
        builder.show();
    }


}
